import EE from 'onfire.js';
import { Mesh, MeshPhysicalMaterial } from 'three';
import { STLLoader } from 'three/examples/jsm/loaders/STLLoader';

/* Object for STL importation */

export class STLSolid extends Mesh {
  public emitter: any;

  constructor(loader: STLLoader, url: string) {
    super();
    this.emitter = new EE();
    loader.load(url, (geometry: any) => {
      const material = new MeshPhysicalMaterial({ color: '#FFFFFF' });

      this.geometry = geometry;
      this.material = material;
      this.emitter.emit('loaded');
    });
  }
}
